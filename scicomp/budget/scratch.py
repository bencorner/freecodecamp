import budget
from budget import create_spend_chart

#abc = [food, clothing, auto]
food = budget.Category("Food")
entertainment = budget.Category("Entertainment")
business = budget.Category("Business")

food.deposit(900, "deposit")         
entertainment.deposit(900, "deposit")
business.deposit(900, "deposit")     
food.withdraw(105.55)                
entertainment.withdraw(33.40)        
business.withdraw(10.99)




cats = [business, food, entertainment]
balances = [cat.get_balance() for cat in cats]

#['amount' in entry.values() if 'deposit' in entry.values() for entry in food.ledger]
expenses = [(900-balance) for balance in balances]
total_spending = sum(expenses)

fractional_spending = [amount/total_spending for amount in expenses]
percentage_of_spending = [100*value for value in fractional_spending]

pct_spd = [100*value for value in fractional_spending]




# food.deposit(900, "deposit")         
# entertainment.deposit(900, "deposit")
# business.deposit(900, "deposit")     
# food.withdraw(105.55)                
# entertainment.withdraw(33.40)        
# business.withdraw(10.99)

actual = create_spend_chart([business, food, entertainment])
#actual = create_spend_chart([self.business, self.food, self.entertainment])


# expected = "Percentage spent by category\n100|          \n 90|          \n 80|          \n 70|    o     \n 60|    o     \n 50|    o     \n 40|    o     \n 30|    o     \n 20|    o  o  \n 10|    o  o  \n  0| o  o  o  \n    ----------\n     B  F  E  \n     u  o  n  \n     s  o  t  \n     i  d  e  \n     n     r  \n     e     t  \n     s     a  \n     s     i  \n           n  \n           m  \n           e  \n           n  \n           t  "

