from itertools import *



class Category:
    def __init__(self, name):
        """Category initialization method"""
        self.name = name
        self.ledger = []
        
        


    # def __repr__(self):
    #     return self.__str__
    #     #return f"{self.name:*^30}"
    #     #return f"*^30{self.name}"


    def __str__(self):
        self_name = f"{self.name:*^30}\n"
        ledger_items = [(entry['description'], entry['amount']) for entry in self.ledger]
        the_ledger = [f"{a:<23.23s}{b:>7.2f}\n" for a, b in ledger_items]

        self_str = f"{self.name:*^30}\n"
        for entry in the_ledger:
            self_str += entry
        
        self_total = self.get_balance()
        self_str += f"Total: {self_total:<6.2f}"

        return self_str
    
# #        print(a, b) in ledger_items
#         #f"{self.ledger}"
#         return f"{self_name}{}"
#         #ledger_strings = [f"{ledger_items}"]
        
#         return self_name
# #        self_items = 

       #return f"*^30{self.name}"
    
    def testdick(self):
        fd = list(self.ledger)
        desc = [dic['description'] for dic in fd]

#        the_ledger = [f"{a:<23} {b:>7.2f}" for a, b in ledger_items]
        the_ledger = [f"{a:<23.23s} {b:>7.2f}" for a, b in ledger_items]
    
    def deposit(self, amount, description=""):
        """A `deposit` method that accepts an amount and description.
        If no description is given, it should default to an empty string. 
        The method should append an object to the ledger list in the
        form of `{"amount": amount, "description": description}`."""
        
        self.ledger.append({"amount": amount, "description": description})
            


    def withdraw(self, amount, description=""):
        """A `withdraw` method that is similar to the `deposit` method, but
        the amount passed in should be stored in the ledger as a negative
        number. If there are not enough funds, nothing should be added to the
        ledger. This method should return `True` if the withdrawal took place,
        and `False` otherwise."""
        
        balance = self.get_balance()
        have_money = self.check_funds(amount)
        if not have_money:
            return False
        else:
            self.ledger.append({"amount": -amount, "description": description})
            return True


    def get_balance(self):
        """A `get_balance` method that returns the current balance of the budget category based on the deposits and withdrawals that have occurred."""

        transactions = [transaction["amount"] for transaction in self.ledger]               
#        print(f"The list of transaction values: {transactions}")
        balance = sum(transactions)
        return balance
            

    def transfer(self, amount, category):
        """A `transfer` method that accepts an amount and another budget
        category as arguments. The method should add a withdrawal with
        the amount and the description "Transfer to [Destination Budget
        Category]". The method should then add a deposit to the other budget
        category with the amount and the description "Transfer from [Source
        Budget Category]". If there are not enough funds, nothing should be
        added to either ledgers. This method should return `True` if the
        transfer took place, and `False` otherwise."""

        have_enough = self.check_funds(amount)
        if have_enough:
            self.withdraw(amount, description=f"Transfer to {category.name}")
            category.deposit(amount, description=f"Transfer from {self.name}")
            return True
        else:
            return False


    def check_funds(self, amount):
        """A `check_funds` method that accepts an amount as an argument.
        It returns `False` if the amount is less than the balance of the
        budget category and returns `True` otherwise. This method should
        be used by both the `withdraw` method and `transfer` method."""
        
        return (amount <= self.get_balance())









        


def create_spend_chart(cats):
    
    balances = [cat.get_balance() for cat in cats]
    expenses = [(900-balance) for balance in balances]
    total_spending = sum(expenses)
    
    fractional_spending = [amount/total_spending for amount in expenses]
    pct_spd = [100*value for value in fractional_spending]
    
    title = "Percentage spent by category\n"
    
    graph = [title]

    for x in range(100,-10,-10):
        # OH SHIT my hideous f-string is working
        graph.append(f"{x:>3}| {('o' if pct_spd[0] >= x else ' ')}  {('o' if pct_spd[1] >= x else ' ')}  {('o' if pct_spd[2] >= x else ' ')}  \n")

    the_str = "".join(graph)
       
    names = [cat.name for cat in cats]
    zipnames = zip_longest(*names, fillvalue=" ")

    x_ax = "    ----------"
    the_graph = the_str + x_ax

    for (a, b, c) in zipnames:                  
        the_graph += (f"\n     {a}  {b}  {c}  ")
    
#    print(the_graph)
    
    return the_graph

'''

result = create_spend_chart(cats)

print(*result) # didn't quite work

the_str = "".join(result) #huzzah, it worked?

x_ax = "    ----------\n"

the_graph = the_str + x_ax



'''

    # for cat in categories:
    #     print(f"This cat is: {cat.name}")
    #     print(cat)
    


def vert_print(cats):
# ['Business', 'Food', 'Entertainment']
    names = [cat.name for cat in cats]
    zipnames = zip_longest(*names, fillvalue=" ")

    
    for (a, b, c) in noms:            
        print(f"     {a}  {b}  {c}  ")
    

    return zipnames
    
    
def print_axes():
    title = "Percentage spent by category"
    print(title)
    graph = []

    for x in range(100,0,-10):
        #print(f"{x:>3}|")
        graph.append(f"{x:>3}|")
#        graph."{x:>3}|"
#        col0 

    return graph
    
