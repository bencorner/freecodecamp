import copy
import random
# Consider using the modules imported above.


kw = {'orange':3, 'blue':2, 'red':4}
# tst = Hat(blue=3, red=1, yellow=5, black=2)



class Hat:
    def __init__(self, **kwargs):

        self.contents = [key for key in kwargs.keys() for i in range(kwargs[key])]

        # self.contents = []
        # for key,value in kwargs.items():
        #     for i in range(value):
        #         self.contents.append(key)


    def draw(self, to_draw):
        if to_draw >= len(self.contents):
            draw_result = self.contents.copy()
            self.contents.clear()
        
        else:
            draw_result = random.sample(self.contents, to_draw)
            for val in draw_result:
                self.contents.remove(val)

        return draw_result
        
    #    return random.sample(self.contents, to_draw)
    

        
def experiment(hat, expected_balls, num_balls_drawn, num_experiments):
    num_success = 0

    for trial in range(num_experiments):

        cp_hat = copy.deepcopy(hat)
        
        draw_result = cp_hat.draw(num_balls_drawn)
        
        count = {color: draw_result.count(color) for color in expected_balls.keys()}


        if not any([expected_balls[key] > count[key] for key in expected_balls.keys()]):
            num_success +=1

    return (num_success/num_experiments)
            # [deez[key] > count[key] for key in deez.keys()]

# def how(hat, expected_balls, num_balls_drawn, num_experiments=1):
#     for trial in range(num_experiments):
#         cp_hat = copy.deepcopy(hat)
        
#         draw_result = cp_hat.draw(num_balls_drawn)
#         print(draw_result)
    
#         count = {color: draw_result.count(color) for color in expected_balls.keys()}
#         print("count: ", count)

#         comp = [expected_balls[key] > count[key] for key in expected_balls.keys()]
#         print(comp)

    # for color,num in expected_balls.items():
    #     pass


    # return (draw_result, {color: draw_result.count(color) for color in expected_balls.keys()})
    




#    {key:draw_result.count(key) for key in expected_balls.keys() }

    # return {key:draw_result.count(key) for key in expected_balls.keys() }

    # for trial in range(num_experiments):
    #     draw_result = cp_hat.draw(num_balls_drawn)

    #     trial_count = {key:draw_result.count(key) for key in expected_balls.keys()}

    # print(draw_result)
    # return {key:draw_result.count(key) for key in expected_balls.keys() }

