

days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]


def add_time(start, duration, weekday=None):
    startstr, ampm = start.split()
    
    start_hrs, start_mins = get_hrs_mins(startstr)
    delta_hrs, delta_mins = get_hrs_mins(duration)

    start_hrs_24 = start_hrs if (ampm == 'AM' or start_hrs==12) else start_hrs+12
    #print("Start hours-time in 24hr format" , start_hrs_24)
    
    
    num_hrs_later = delta_hrs + (delta_mins + start_mins) // 60
    print(f"Number of hours later: {num_hrs_later}")
    num_days_later = (start_hrs_24 + num_hrs_later) // 24
    print(f"Number of days later: {num_days_later}")

    new_hr = (start_hrs_24 + num_hrs_later) % 24
    print(f"new_hr: {new_hr}")
    new_mn = (start_mins + delta_mins) % 60
    new_ampm = "AM" if new_hr < 12 else "PM"

    if new_hr == 0 or new_hr == 12:
        new_hr = 12
    else:
        new_hr = new_hr % 12

    new_time = f"{new_hr}:{new_mn:02} {new_ampm}"    
    # new_time = f"{new_hr % 12 if new_hr != 12 else 12 }:{new_mn:02} {new_ampm}"

    if weekday:
        day_index = days.index(weekday.capitalize())
        new_time += f", {days[(day_index + num_days_later)%7]}" 

    if num_days_later == 1:
        new_time += f" (next day)"
    elif num_days_later > 1:
        new_time += f" ({num_days_later} days later)"

    return new_time


def get_hrs_mins(timestr):
    """For time strings of form `hh:mm` """

    t_hrs = timestr[:-3]
    t_mins = timestr[-2:]
    

    return int(t_hrs), int(t_mins)
