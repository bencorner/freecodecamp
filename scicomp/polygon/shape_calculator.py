class Rectangle():
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def __repr__(self):
        return f"Rectangle(width={self.width}, height={self.height})"
    
        
    def set_width(self, width):
        self.width = width

    def set_height(self, height):
        self.height = height


    def get_area(self):
        return (self.width * self.height)

    def get_perimeter(self):
        return (2 * (self.width + self.height))

    def get_diagonal(self):
        return ( (self.width ** 2 + self.height ** 2) ** 0.5)

    def get_picture(self):
        if (self.width > 50 or self.height > 50):
            return "Too big for picture."
        
        rect_pict = f"{'*' * self.width}\n" * self.height
        return rect_pict

    def get_amount_inside(self, shape_inside):
        width_inside = self.width // shape_inside.width
        height_inside = self.height // shape_inside.height
        
        return width_inside * height_inside
    

    

        
class Square(Rectangle):
    # hmm (self, side, side) or (,w=s,h=s) or (self,side)??
    def __init__(self, side):
        self.side = side
        super().__init__(side, side)

    def __repr__(self):
        return f"Square(side={self.width})"

    def set_side(self, side):
        self.side = side
        super().set_width(side)
        super().set_height(side)


    def set_width(self, width):
        self.set_side(width)

    def set_height(self, height):
        self.set_side(height)
        
    pass
